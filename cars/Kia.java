package domain.cars;

import domain.abstracts.Car;

public class Kia extends Car {

    public Kia(String model, String color, double engineVolume) {
        super(model, color, engineVolume);
    }

}
