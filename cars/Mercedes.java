package domain.cars;

import domain.abstracts.Car;

public class Mercedes extends Car {

    public Mercedes(String model, String color, double engineVolume) {
        super(model, color, engineVolume);
    }

}
