package domain.cars;

import domain.abstracts.Car;

public class Toyota extends Car {

    public Toyota(String model, String color, double engineVolume) {
        super(model, color, engineVolume);
    }
}
