package domain.abstracts;

import domain.abstracts.Car;
import domain.cars.Kia;
import domain.interfaces.CarAbstractFactory;

public abstract class ACarFactory<T> implements CarAbstractFactory {
    private String color,model;
    private double engineVolume;
    @Override
    public Car createCar(){
        return new Kia(model,color,engineVolume);
    };
    public T setModel(String model){
        this.model = model;
        return (T) this;
    };
    public T setColor(String color){
        this.color = color;
        return (T) this;

    };
    public T setEngineVolume(double engineVolume){
        this.engineVolume = engineVolume;
        return (T) this;
    };
}
