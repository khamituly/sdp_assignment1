package domain.abstracts;

public abstract class Car {

    private String model, color;
    private double engineVolume;

    public Car(String model, String color, double engineVolume) {
        setModel(model);
        setColor(color);
        setEngineVolume(engineVolume);
    }

    public Car() {

    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getEngineVolume() {
        return engineVolume;
    }

    public void setEngineVolume(double engineVolume) {
        this.engineVolume = engineVolume;
    }

    @Override
    public String toString() {
        return model + ',' + color + ',' + engineVolume;
    }

}
