package domain;

import domain.abstracts.Car;
import domain.interfaces.CarAbstractFactory;

public class CarFactory {

    public static Car createCar(CarAbstractFactory car) {
        return car.createCar();
    }


}
