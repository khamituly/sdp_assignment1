package domain.interfaces;

import domain.abstracts.Car;

public interface CarAbstractFactory {
    public Car createCar();
}
