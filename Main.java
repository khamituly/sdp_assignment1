package com.company;

import domain.abstracts.Car;
import domain.CarFactory;
import domain.carFactories.KiaFactory;
import domain.carFactories.MercedesFactory;
import domain.carFactories.ToyotaFactory;

public class Main {

    public static void main(String[] args) {

        Car Toyota = CarFactory.createCar(new ToyotaFactory().setEngineVolume(5.5).setColor("grey").setModel("Hilux"));

        System.out.println(Toyota);

        Car Mercedes = CarFactory.createCar(new MercedesFactory().setEngineVolume(3.5).setModel("AMG"));

        System.out.println(Mercedes);

        Car Kia = CarFactory.createCar(new KiaFactory().setModel("Cerato").setEngineVolume(2));

        System.out.println(Kia);

        Car Kia2 = CarFactory.createCar(new KiaFactory().setModel("Rio").setColor("green"));

        System.out.println(Kia2);
    }
}
